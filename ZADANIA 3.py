#!/usr/bin/env python3

import random

# Task 1
# 1

a = random.randint(1, 50)
print(a)

while a != 20:
    a = random.randint(1, 100)
    print(a)

# 2

print("-" * 20)
for number in range(20, -1, -1):
    print(number)

print("-" * 20)

# Task 2
# 1

tree_list = [[1], [3], [4]]
print(tree_list)

# 2

number_list = []

for i in range(100, 116):
    number_list.append(i)

print("List:", number_list)
print("-" * 10)

# 3

float_list = [1.5, 2.5, 5.1, 6.8, 10.9]

for i in float_list:
    print(i)

print("-" * 10)

# Task 3
# 1

list1 = []

for i in range(100):
    list1.append(i)

tuple_list = tuple(list1)
print(tuple_list)
print("-" * 10)

# 2
five_list = (1, 2, 3, 4, 5)
a, b, c, d, e = five_list

print(five_list)
print("-" * 10)

# Task 4
# 1
user_dictionary = {}

for i in range(5):
    enter_key = input("Give key:")

    while enter_key in user_dictionary:
        enter_key = input("Give unique key:")

    enter_value = input("Give value:")
    user_dictionary[enter_key] = enter_value

print(user_dictionary)

# 2
list_dictionary = {"flowers": ["tulip", "rose", "daisy", "cactus", "snowdrop"],
                   "animals": ["cat", "dog", "pig", "cow", "bird"],
                   "furniture": ["chair", "table", "desk", "wardrobe", "bookshelf"],
                   "name": ["Gabriel", "Conrad", "Thomas", "Mark", "Agnes"],
                   "meals": ["chicken", "soup", "pizza", "fish", "kebab"]}

print(list_dictionary)